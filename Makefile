reboot:
ifeq ($(IS_AARCH64),y)
	$(CC) -I. main.c reboot.c android_reboot.c ./arm64/__set_errno.cpp ./arm64/memset.S ./arm64/syscalls/__reboot.S -o reboot
else
	$(CC) -I. main.c reboot.c android_reboot.c ./arm/__set_errno.c ./arm/syscalls/__reboot.S -o reboot
endif

install:
	install -m 755 reboot $(DESTDIR)/usr/sbin

clean:
	-rm reboot

all: reboot
